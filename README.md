# Layout-Resources-in-Android
public class HelloWorldActivity extends Activity
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		TextView tv = (TextView)this.findViewById(R.id.text1);
		 tv.setText("Try this text instead");
	}
